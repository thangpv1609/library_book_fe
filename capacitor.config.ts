import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'library_book',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
