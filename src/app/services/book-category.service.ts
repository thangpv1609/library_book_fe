import { Injectable } from '@angular/core';
import { BookCategory } from '../interfaces/book-category';
import { AxiosService } from './axios.service';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class BookCategoryService {
  protected axiosService = new AxiosService()
  protected url = '/book-category/'
  protected toastService = new ToastService()

  constructor() { }

  async search(id?: number, limit?: number, offset?: number) {
    if(id) {
      this.url += id
    }
    const response = await this.axiosService.get(this.url)
    return response
  }

  async create(formBookCategory: BookCategory) {
    const response = this.axiosService.post(this.url, formBookCategory)
    if(typeof response === 'object' && response !== null && response.hasOwnProperty('id')) {
      this.toastService.create(`Đầu sách "${formBookCategory.name}" đã được tạo!`)
    }
    else {
      this.toastService.create(`Chưa tạo được đầu sách "${formBookCategory.name}"`)
    }
  }

  update(formBookCategory: BookCategory) {
    const response = this.axiosService.put(this.url + '/', formBookCategory)
  }

  delete() {
    const response = this.axiosService.delete(this.url + '/')
  }
}
