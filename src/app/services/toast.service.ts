import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class ToastService {
  protected toastController = new ToastController()
  
  constructor() { }

  async create(message: string, duration = 2000) {
    const toast = await this.toastController.create({
      message: message,
      duration: duration,
    });
    toast.present();
  }
}
