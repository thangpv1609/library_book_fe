import { Injectable } from '@angular/core';
import axios from 'axios';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AxiosService {
  baseURL = 'http://localhost:8000'
  localStorageService = new LocalStorageService()
  instance = axios.create({
    baseURL: this.baseURL,
  });
  
  constructor() {
    const ACCESS_TOKEN = this.localStorageService.getValue('access')
    if(ACCESS_TOKEN) {
      this.instance.defaults.headers.common['Authorization'] = 'Bearer ' + ACCESS_TOKEN;
    }
  }

  async get(url: string) {
    const response = await this.instance.get(url)
    if(response.statusText == 'OK') {
      return response.data
    }
  }

  async post(url: string, data: object) {
    const response = await this.instance.post(url, data)
    if(response.statusText == 'OK') {
      return response.data
    }
  }

  async put(url: string, data: object) {
    const response = await this.instance.put(url, data)
    if(response.statusText == 'OK') {
      return response.data
    }
  }

  async delete(url: string) {
    const response = await this.instance.delete(url)
    if(response.statusText == 'OK') {
      return response.data
    }
  }
}
