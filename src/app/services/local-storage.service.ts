import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  getValue(itemName:string) {
    const item = localStorage.getItem(itemName);
    return  item || false
  }

  setValue(itemName:string, itemValue:string) {
    localStorage.setItem(itemName, itemValue);
  }
}
