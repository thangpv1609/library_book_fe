import { Injectable } from '@angular/core';
import { AxiosService } from './axios.service';
import { Book } from '../interfaces/book';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  protected axiosService = new AxiosService()
  protected toastService = new ToastService()

  url = '/book/'

  constructor() { }

  async search(id?: number, limit?: number, offset?: number) {
    if(id) {
      this.url += id
    }
    const response = await this.axiosService.get(this.url)
    return response
  }

  async create(formBook: Book) {
    const response = this.axiosService.post(this.url, formBook)
    if(typeof response === 'object' && response !== null && response.hasOwnProperty('id')) {
      this.toastService.create(`Đầu sách "${formBook.name}" đã được tạo!`)
    }
    else {
      this.toastService.create(`Chưa tạo được đầu sách "${formBook.name}"`)
    }
  }

  update(formBook: Book) {
    const response = this.axiosService.put(this.url + '/', formBook)
  }

  delete() {
    const response = this.axiosService.delete(this.url + '/')
  }

}
