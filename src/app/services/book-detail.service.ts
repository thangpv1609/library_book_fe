import { Injectable } from '@angular/core';
import { BookDetail } from '../interfaces/book-detail';
import { AxiosService } from './axios.service';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class BookDetailService {
  protected axiosService = new AxiosService()
  protected toastService = new ToastService()

  url = '/book-detail/'

  constructor() { }

  async search(id?: number, limit?: number, offset?: number) {
    if(id) {
      this.url += id
    }
    const response = await this.axiosService.get(this.url)
    return response
  }

  async create(formBookDetail: BookDetail) {
    const response = this.axiosService.post(this.url, formBookDetail)
    if(typeof response === 'object' && response !== null && response.hasOwnProperty('id')) {
      this.toastService.create(`Đầu sách "${formBookDetail.code}" đã được tạo!`)
    }
    else {
      this.toastService.create(`Chưa tạo được đầu sách "${formBookDetail.code}"`)
    }
  }

  update(formBookDetail: BookDetail) {
    const response = this.axiosService.put(this.url + '/', formBookDetail)
    return response
  }

  delete() {
    const response = this.axiosService.delete(this.url + '/')
    return response
  }

}
