import { Injectable } from '@angular/core';
import { AxiosService } from './axios.service';
import { LocalStorageService } from './local-storage.service';
import { ToastService } from './toast.service';

export interface FormLogin {
  username: string,
  password: string,
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  protected axiosService = new AxiosService()
  protected localStorageService = new LocalStorageService()
  protected toastService = new ToastService()
  
  constructor() { }

  async login(formLogin: FormLogin) {
    const response = await this.axiosService.post('/login/', formLogin)
    if(typeof response === 'object' && response !== null) {
      this.localStorageService.setValue('access', response.access)
      this.localStorageService.setValue('refresh', response.refresh)
      this.toastService.create('Đăng nhập thành công')
    }
  }

  async refreshToken() {
    const refreshToken = this.localStorageService.getValue('refresh')
    if(refreshToken) {
      const response = await this.axiosService.post('/api/token/refresh/', {"refresh": refreshToken})
      if(response.hasOwnProperty('access')) {
        this.localStorageService.setValue('access', response.access)
      }
    }
  }
}

