import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'form-book',
    loadChildren: () => import('./pages/form-book/form-book.module').then( m => m.FormBookPageModule)
  },
  {
    path: 'form-book-detail',
    loadChildren: () => import('./pages/form-book-detail/form-book-detail.module').then( m => m.FormBookDetailPageModule)
  },
  {
    path: 'list-user',
    loadChildren: () => import('./pages/list-user/list-user.module').then( m => m.ListUserPageModule)
  },
  {
    path: 'list-book-detail',
    loadChildren: () => import('./pages/list-book-detail/list-book-detail.module').then( m => m.ListBookDetailPageModule)
  },
  {
    path: 'form-book-category',
    loadChildren: () => import('./pages/form-book-category/form-book-category.module').then( m => m.FormBookCategoryPageModule)
  },
  {
    path: 'list-book-category',
    loadChildren: () => import('./pages/list-book-category/list-book-category.module').then( m => m.ListBookCategoryPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
