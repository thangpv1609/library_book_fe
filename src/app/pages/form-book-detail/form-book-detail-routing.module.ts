import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormBookDetailPage } from './form-book-detail.page';

const routes: Routes = [
  {
    path: '',
    component: FormBookDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormBookDetailPageRoutingModule {}
