import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormBookDetailPageRoutingModule } from './form-book-detail-routing.module';

import { FormBookDetailPage } from './form-book-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormBookDetailPageRoutingModule
  ],
  declarations: [FormBookDetailPage]
})
export class FormBookDetailPageModule {}
