import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormBookPageRoutingModule } from './form-book-routing.module';

import { FormBookPage } from './form-book.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormBookPageRoutingModule
  ],
  declarations: [FormBookPage]
})
export class FormBookPageModule {}
