import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Book } from 'src/app/interfaces/book';
import { BookCategory } from 'src/app/interfaces/book-category';
import { BookCategoryService } from 'src/app/services/book-category.service';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-form-book',
  templateUrl: './form-book.page.html',
  styleUrls: ['./form-book.page.scss'],
})
export class FormBookPage implements OnInit {
  protected book: Book
  protected listCategory: Array<BookCategory>
  protected bookCategoryService = new BookCategoryService()
  protected bookService = new BookService()
  protected book_id: number | undefined

  constructor(private route: ActivatedRoute) {
    this.book = {
      id: null,
      code: '',
      name: '',
      description: '',
      book_category: 0,
    }
    this.listCategory = []
  }

  async ngOnInit() {
    const response = await this.bookCategoryService.search()
    this.listCategory = response.results
    this.route.queryParams.subscribe(params => {
      this.book_id = params['id']
    });
    if(this.book_id) {
      const response = await this.bookService.search(this.book_id)
      this.book = response
    }
  }

  onCreate() {
    this.bookService.create(this.book)
  }

  onUpdate() {
    if(this.book_id) {
      this.bookService.update(this.book)
    }
  }

  onDelete() {
    if(this.book_id) {
      this.bookService.delete()
    }
  }

}
