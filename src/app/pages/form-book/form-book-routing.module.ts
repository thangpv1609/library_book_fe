import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormBookPage } from './form-book.page';

const routes: Routes = [
  {
    path: '',
    component: FormBookPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormBookPageRoutingModule {}
