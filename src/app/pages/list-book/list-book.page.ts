import { Component } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Book } from '../../interfaces/book';

@Component({
  selector: 'app-list-book',
  templateUrl: 'list-book.page.html',
  styleUrls: ['list-book.page.scss']
})
export class ListBookPage {
  bookService = new BookService()
  books: Array<Book>

  constructor() {
    this.books = []
  }

  async ngOnInit() {
    const response = await this.bookService.search()
    this.books = response.data
  }

  handleRefresh(event: any) {
    setTimeout(() => {
      // Any calls to load data go here
      event.target.complete();
    }, 2000);
  };
}
