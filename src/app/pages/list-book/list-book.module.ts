import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ListBookPage } from './list-book.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { ListBookPageRoutingModule } from './list-book-routing.module';
import { SlideMenuComponent } from '../../components/slide-menu/slide-menu.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    ListBookPageRoutingModule
  ],
  declarations: [ListBookPage, SlideMenuComponent]
})
export class ListBookPageModule {}
