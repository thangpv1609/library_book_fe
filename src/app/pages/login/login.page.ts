import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  protected username: string;
  protected password: string;
  protected authenticationService = new AuthenticationService()

  constructor() {
    this.username = ''
    this.password = ''
  }

  ngOnInit() { }

  onSubmit() {
    this.authenticationService.login({
      username: this.username,
      password: this.password
    })
  }

  onRefreshToken() {
    this.authenticationService.refreshToken()
  }
}
