import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormBookCategoryPage } from './form-book-category.page';

const routes: Routes = [
  {
    path: '',
    component: FormBookCategoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormBookCategoryPageRoutingModule {}
