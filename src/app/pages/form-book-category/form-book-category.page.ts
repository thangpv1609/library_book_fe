import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookCategory } from 'src/app/interfaces/book-category';
import { BookCategoryService } from 'src/app/services/book-category.service';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-form-book-category',
  templateUrl: './form-book-category.page.html',
  styleUrls: ['./form-book-category.page.scss'],
})
export class FormBookCategoryPage implements OnInit {
  protected bookCategory: BookCategory
  protected bookCategoryService = new BookCategoryService()
  protected bookCategoryId: number | undefined

  constructor(private route: ActivatedRoute) {
    this.bookCategory = {
      id: null,
      name: '',
      description: '',
    }
  }

  async ngOnInit() {
    if(this.bookCategoryId) {
      const response = await this.bookCategoryService.search(this.bookCategoryId)
      this.bookCategory = response
    }
  }

  onCreate() {
    this.bookCategoryService.create(this.bookCategory)
  }

  onUpdate() {
    if(this.bookCategoryId) {
      this.bookCategoryService.update(this.bookCategory)
    }
  }

  onDelete() {
    if(this.bookCategoryId) {
      this.bookCategoryService.delete()
    }
  }
}
