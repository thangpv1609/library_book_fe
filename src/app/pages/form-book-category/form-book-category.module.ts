import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormBookCategoryPageRoutingModule } from './form-book-category-routing.module';

import { FormBookCategoryPage } from './form-book-category.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormBookCategoryPageRoutingModule
  ],
  declarations: [FormBookCategoryPage]
})
export class FormBookCategoryPageModule {}
