import { Component, OnInit } from '@angular/core';
import { BookCategory } from 'src/app/interfaces/book-category';
import { BookCategoryService } from 'src/app/services/book-category.service';

@Component({
  selector: 'app-list-book-category',
  templateUrl: './list-book-category.page.html',
  styleUrls: ['./list-book-category.page.scss'],
})
export class ListBookCategoryPage implements OnInit {
  bookCategoryService = new BookCategoryService()
  bookCategorys: Array<BookCategory>

  constructor() {
    this.bookCategorys = []
  }

  async ngOnInit() {
    const response = await this.bookCategoryService.search()
    this.bookCategorys = response.data
  }
}
