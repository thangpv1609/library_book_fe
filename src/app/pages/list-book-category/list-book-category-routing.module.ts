import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListBookCategoryPage } from './list-book-category.page';

const routes: Routes = [
  {
    path: '',
    component: ListBookCategoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListBookCategoryPageRoutingModule {}
