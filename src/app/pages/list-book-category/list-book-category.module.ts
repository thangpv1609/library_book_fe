import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListBookCategoryPageRoutingModule } from './list-book-category-routing.module';

import { ListBookCategoryPage } from './list-book-category.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListBookCategoryPageRoutingModule
  ],
  declarations: [ListBookCategoryPage]
})
export class ListBookCategoryPageModule {}
