import { Component, OnInit } from '@angular/core';
import { BookDetail } from 'src/app/interfaces/book-detail';
import { BookDetailService } from 'src/app/services/book-detail.service';

@Component({
  selector: 'app-list-book-detail',
  templateUrl: './list-book-detail.page.html',
  styleUrls: ['./list-book-detail.page.scss'],
})
export class ListBookDetailPage implements OnInit {
  bookDetailService = new BookDetailService()
  bookDetails: Array<BookDetail>

  constructor() {
    this.bookDetails = []
  }

  async ngOnInit() {
    const response = await this.bookDetailService.search()
    this.bookDetails = response.data
  }
}
