import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListBookDetailPageRoutingModule } from './list-book-detail-routing.module';

import { ListBookDetailPage } from './list-book-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListBookDetailPageRoutingModule
  ],
  declarations: [ListBookDetailPage]
})
export class ListBookDetailPageModule {}
