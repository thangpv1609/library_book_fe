import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListBookDetailPage } from './list-book-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ListBookDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListBookDetailPageRoutingModule {}
