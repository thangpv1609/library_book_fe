export interface Book {
    id: number | null,
    code: string,
    name: string,
    description: string,
    book_category: number,
}
