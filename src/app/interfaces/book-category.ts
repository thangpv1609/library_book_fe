export interface BookCategory {
    id: number | null,
    name: string,
    description: string,
}